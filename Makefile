TEXS=\
tex/abstract.tex tex/acknowledgements.tex tex/appendix.tex \
tex/conclusions.tex tex/defs.tex tex/intro.tex \
tex/methods.tex tex/packages.tex tex/results.tex \
tex/setup.tex tex/software.tex tex/title.tex \
tex/valid.tex

BIBS=bib/dpd.bib bib/libraryAE.bib bib/extra.bib
IMGS=\
tex/figures/validation.pdf tex/figures/dv_ht45.pdf tex/figures/microB_all.pdf \
tex/figures/intime_BE_auto+1.pdf tex/figures/params.pdf

STY=\
sty/Shangda1226.sty

DEPS=$(TEXS) $(BIBS) $(IMGS) $(STY)

main.pdf: main.tex ${DEPS}
	pdflatex main.tex && \
	bibtex   main     && \
	pdflatex main.tex && \
	pdflatex main.tex

.PHONY: clean
clean:
	rm -f \
	main.aux main.bbl main.blg \
	main.log main.out main.pdf

#!/bin/bash

# Update bibtex files
t=/tmp/ab.$$.bib
tools=../../../tools

for f in dpd.bib  extra.bib  libraryAE.bib
do
    $tools/jabrv.awk $tools/abbrv "$f" > "$t" &&
	mv "$t" "$f"
done

\section{Numerical Methods} \label{sec:me}

\subsection{Dissipative Particle Dynamics}
Following recent works~\supercite{Fedosov:2011a, Rossinelli:2015}, we model blood as a suspension of RBCs using the Dissipative Particle Dynamics.
DPD is a mesoscopic method which assumes a coarse grained representation of the system: the number of degrees of freedom is reduced by representing clusters of molecules with soft particles, to simulate fluids on a mesoscopic scale~\supercite{FedosovDA2012}.
Essentially it is an N-body algorithm where discrete particles interact through pairwise forces.
We denote by $\vect{r}_{ij} = \vect{r}_i - \vect{r}_j$ the vector between particle centers $i$ and $j$, $\vect{e}_{ij} = \vect{r}_{ij}/r_{ij}$, and by $\vect{v}_{ij} = \vect{v}_i - \vect{v}_j$ the relative velocity.
The particle's position and velocity are updated through Newton's second law:
$m_i\frac{d\vect{v}_{i}}{dt} = \vect{f}_i$,
where $\vect{f}_i$ is the total force applied on particle $i$.
It is a sum of three forces each of which is pairwise additive~\supercite{Groot1997}.
\begin{equation*}
\vect{f}_i = \sum_{i \neq j} \left( \Fc + \Fd + \Fr \right) \vect{e}_{ij}
\end{equation*}

The conservative force, $\Fc$, is a soft repulsion between two particles.
The dissipative force, $\Fd$, represents the energy loss caused by frictional effects.
The random force, $F_{ij}^R$, represents the energy increase by including the effect of the suppressed degrees of freedom in the form of thermal fluctuations.
\begin{equation*}
\Fc = a w^C (r_{ij}),
\quad
\Fd = \gamma w^D (r_{ij}) \left(\vect{r}_{ij} \cdot \vect{v}_{ij}\right),
\quad
\Fr = \sigma w^R \left( r_{ij} \right) \zeta_{ij} \Delta t^{-1/2}
\end{equation*}
$w^D$, $w^R$, and $w^C$ are weight functions that are zero for $r > r_c = 1$, and $\zeta_{ij}$ is a Gaussian random variable with zero mean and unit variance.

We choose the following weight functions:
\begin{equation*}
  w^C(r_{ij}) = 1 - r_{ij},
  \quad
  w^D(r_{ij}) = (w^R(r_{ij}))^2 = \left( 1 - r_{ij} \right)^{k}
\end{equation*}
where $k$ is an envelop parameter.
The connection between random and dissipative forces is set by the fluctuation-dissipation theorem~\supercite{Espanol1995a}: $\sigma^2 = 2 \gamma k_B T$, where $k_B$ is the Boltzmann constant and $T$ is the temperature.

\subsection{RBC model}
The RBC membrane is represented by DPD particles composing the vertices of a two-dimensional triangulated network~\supercite{FedosovDA2012}.
Incident mesh triangles have an associated potential energy depending on the angle between them, and local and global area constraints are enforced, along with a global volume constraint.

The total potential energy of the system is~\supercite{Fedosov2010c}
\begin{equation*}
U = \Uip + \Ube + \Uar + \Uvo
\end{equation*}
$\Uip$ accounts for the energy of the elastic spectrin network of the RBC membrane, including an attractive wormlike chain potential and a repulsive potential
\begin{equation*}
\Uip = \sum_{j \in {1 ... N_s}}
\left[
  \frac
  {
    k_BT l_m
    \left(
      3x_j^2 - 2x_j^3
    \right)
  }
  {4p(1-x_j)}
  +
  \frac{k_p}{l_0}
\right]
\end{equation*}
where $x_j$ is the normalized spring length and $N_s$ is the number of springs.

The bending energy term, $\Ube$, models the bending resistance of the lipid bilayer and is equal to
\begin{equation} \label{eq:bending}
\Ube = \sum_{j \in {1 ... N_s}} k_b
\left[
  1-\cos(\theta_j - \theta_0)
\right]
\end{equation}
where $k_b$ is a bending constant and $\theta_j$ is the angle between two adjacent triangles.

$\Uar$ and $\Uvo$ represent the area and volume conservation constraints, which mimic the area-incompressibility of the lipid bilayer and the incompressibility of the inner cytosol, respectively.
\begin{equation*}
\Uar = \frac{k_a (\Atot-\Aotot)^2}{2\Aotot}
          +
       \sum_{j \in {1 ... N_t}} \frac{k_d (A_j-A_0)^2}{2A_0} \, ,
%%%
\Uvo = \frac{k_v(V-\Votot)^2}{2\Votot}\, \,
%%%
\Atot = \sum_{j \in {1 ... N_t}} A_j
\end{equation*}

where $A_j$ is the area of one triangle, $V$ is the volume enclosed by the membrane and $N_t$ is the number of triangles used for the discretization of the RBC membrane.
The mass of the membrane particles is $m_m = 0.5$ and the mass of solvent particles is $m_s = 1$. The viscosity in interior of the cell is the same as the viscosity of the solvent. No viscosity of membrane is considered.

\subsection{Wall-RBC and RBC-RBC interactions}
Solid walls are modeled using ``frozen'' particles --- particles which have prescribed zero velocity and are not subject to position update.
Due to the soft DPD interactions, fluid particles are not fully prevented from penetrating the wall.
In order to simulate impenetrable walls, we use a bounce-back boundary condition for both the solvent and solute particles, which re-inserts any crossing particles into the fluid domain, with opposite velocity~\supercite{Revenga1999}.

The interactions between different RBCs are modelled through a short-ranged purely repulsive Lennard-Jones potential in addition to the DPD force field, in order to ensure that there is no overlapping between RBCs.
\begin{equation*}
U_\mathrm{LJ} = 4\epsilon_{LJ}
    \left[
       \left(
          \frac{\sigma_{LJ}}{r}
        \right)^{12}
      -
       \left(
           \frac{\sigma_{LJ}}{r}
        \right)^{6}
    \right]
\end{equation*}
The RBC-solvent interactions are modelled through DPD forces, by choosing the appropriate DPD parameters~\supercite{Altenhoff2007}.

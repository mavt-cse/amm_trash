#!/bin/bash

cpd () { # works like `cp' but create a directory if it does not exist
    for lst; do true; done # get last argument
    mkdir -p "$lst"
    cp "$@"
}

d=../../../overleaf.iccs2017
test -d $d || git clone https://git.overleaf.com/7870104brqsrnnjsgjf $d

cpd README.org Makefile main.tex *.cls     $d
cpd tex/*.tex                              $d/tex
cpd tex/figures/*                          $d/tex/figures
cpd bib/*.bib                              $d/bib

(
    cd $d
    git pull &&
	git add -A . &&
	git commit -m "overleaf-update commit" &&
	git push
)
